[<span style="color: black;">Home</span>](./index.md) | [<span style="color: black;">Project</span>](./project_idea.md) | [<span style="color: black;">Team</span>](./team.md) | [<span style="color: black;">Terminologies</span>](./terminologies.md) | [<span style="color: black;">Progress</span>](./progress.md)


# Young's Double Slit Experiment

Young's double slit experiment is a famous physics experiment that demonstrates the wave-like nature of light.

When light passes through two small holes, it creates a pattern of bright and dark stripes on a wall. This happens because light behaves like waves, spreading out and interfering with each other. This experiment showed that light acts like both waves and particles. When we talk about the Young's double slit experiment, we see a pattern of stripes on a wall when light passes through two tiny holes. These stripes show that light acts like waves. But here's the twist: even when we send single tiny particles of light (called photons) through those holes, they still create the same stripe pattern! It's like each photon is somehow interfering with itself. Plus, when we try to observe which hole a photon goes through, the pattern changes. It's as if the act of observing affects how the photons behave. This shows the weird and fascinating dual nature of light - it behaves like both waves and particles, and observing it can change its behavior.


## Our Inspiration
We are all physics lovers and we are very much obsessed with the physics concept and applying it into real life. This project idea was firstly introduced to us by our technology teacher. After he made some introduction and description about the experiment, we were driven crazy by the concept, and its complexity and the inevitable confusion about the concept made us want to learn more about it. Also the duality of wave particles seemed something very interesting to study about and inspired us to do this as our project. The fact that still no one really knows how it occurs and the reason behind of such occurrence made us very interested and curious about it


[<span style="color: black;">Home</span>](./index.md) | [<span style="color: black;">Project</span>](./project_idea.md) | [<span style="color: black;">Team</span>](./team.md) | [<span style="color: black;">Terminologies</span>](./terminologies.md) | [<span style="color: black;">Progress</span>](./progress.md)

