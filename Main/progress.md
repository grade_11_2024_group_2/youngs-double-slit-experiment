[<span style="color: black;">Home</span>](./index.md) | [<span style="color: black;">Project</span>](./project_idea.md) | [<span style="color: black;">Team</span>](./team.md) | [<span style="color: black;">Terminologies</span>](./terminologies.md) | [<span style="color: black;">Progress</span>](./progress.md)


# Progress

03/05/2024 <br>
In today's class, we delved into designing a storage box to house our project components using Fusion 360. As we worked through the design process, we uncovered several new features within Fusion 360 that enriched our understanding and proficiency with the software. These discoveries not only expanded our skill set but also opened up new avenues for enhancing our box design.

07/05/2024 <br>
Today, we finished designing our box to store the components of our project. In the last class we failed designing the box. But in today's class we were able to design the box by watching a tutorial from youtube. 

### Failed Designs
<img src="../Images/box1.png" alt="Image" width="400" height="400">
<img src="../Images/box.png" alt="Image" width="400" height="400"> 

### Tutorial from Youtube
Click on the image to view the tutorial. 
[![Sample Video](../Images/youtube.jpg)](https://youtu.be/ZrcqauNvt0M?si=27yE_KwqS78-8ln6)

08/06/2024 <br>
Today, we developed a comprehensive roadmap outlining our future objectives. Within our documentation, we constructed a table detailing our goals, action plans, success indicators, and timelines. Subsequently, we exported this roadmap from our documentation platform as a PDF file. Finally, we uploaded the PDF to our project's designated drive folder for easy access and reference.

<img src="../Images/Roadmap.png" alt="Image" width="600" height="600">

10/06/2024 <br>
According to our plan, today we thought of creating a tiny hole in an acryllic material and on our glass slit using the laser cutting machine. But unfortunately, the laser cutting machine was occupied by someone else. Despite that, we tried to jump on our next plan which was to darken the glass slit using a candle, but we couldn't light it because we didn't have anything to ignite it with. Therefore, today's workdone is equivalent to zero. 

11/06/2024 <br>
Today, using the candle provided by Sir Anith, we darkened the slits to block any light from passing through. Then, with a blade, we carefully made two very tiny cuts on each slit to create passages for the laser beam. By the end of the class, we had successfully darkened both slits and created the required openings. Moreover, during our testing, we successfully captured the interference pattern, confirming the success of our experiment.


<img src="../Images/first.jpg" alt="Image" width="400" height="400">

13/06/2024 <br>
During today's learning experience, our primary objective revolved around determining the optimal distance for our experiment. With the guidance of Sir Anith, we experimented with various distances and observed distinct interference patterns. Based on our new findings, we decided against constructing a box due to its resource-intensive nature and the significant length it would require. Instead, we utilized assisting tools such as a holder for our glass slit and a camera stand to stabilize the laser beam. However, we resolved to craft the stand ourselves to refine our design and carpentry skills. By the end of the class, we concluded that the interference pattern's size increases with greater distance. Consequently, for our project, we intend to set the distance at 3 meters.


<img src="../Images/1.jpg" alt="Image" width="400" height="400">

<img src="../Images/3.jpg" alt="Image" width="400" height="400">

<img src="../Images/2.jpg" alt="Image" width="400" height="400">

<img src="../Images/4.jpg" alt="Image" width="400" height="400">


[<span style="color: black;">Home</span>](./index.md) | [<span style="color: black;">Project</span>](./project_idea.md) | [<span style="color: black;">Team</span>](./team.md) | [<span style="color: black;">Terminologies</span>](./terminologies.md) | [<span style="color: black;">Progress</span>](./progress.md)