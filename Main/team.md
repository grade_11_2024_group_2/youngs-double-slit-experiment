[<span style="color: black;">Home</span>](./index.md) | [<span style="color: black;">Projects</span>](./project_idea.md) | [<span style="color: black;">Team</span>](./team.md) | [<span style="color: black;">Terminologies</span>](./terminologies.md) | [<span style="color: black;">Progress</span>](./progress.md)


# Members Information

### Chencho Wangdi
![Chencho](../Images/chencho.png)

Hi, I'm Chencho Wangdi and I'm 17 years old. Right now, I'm in the eleventh grade. My love for sports has always fueled my competitive nature and motivated me to succeed. I love exploring the world of coding in addition to athletics, and I'm always looking for new challenges and chances to learn more. But the vastness of space and the sky are what really fascinate me. I'm always in awe of the marvels of the cosmos, and I'm driven to learn more about the uncharted territories beyond our globe by my intense curiosity. Exploring new things and trying different things will always be part of me. I was mainly involved in finding information about this experiment and all the things that were to do with gathering information and writing works. I anticipate gaining insights into the dual nature of light and its implications through the ongoing experiment. Additionally, I expect to discover the advantages of experimental learning over theoretical approaches. This understanding will likely prove beneficial as I delve deeper into this concept in grade 12.


### Dhan Bdr. Rai
![Dhan](../Images/dhan.png)

I'm Dhan Bdr. Rai, a 19-year-old student from Chhukha Dzongkhag. I'm in grade 11 and I love learning about local medicine and traditional healing methods using herbs. Besides studying, I enjoy playing badminton and volleyball with my friends. When I need a break, I like walking in the forest and feeling close to nature. I also draw pictures and make designs for our school projects. So, I'm a student who loves learning, playing sports, and being in nature. I was mainly involved in drawing illustrations of our project and making designs of our project. I'm eager to learn how the end product of this project works and what mechanisms drive its functionality. Is the slit responsible for creating the pattern, or can other materials like wood be used as replacements? The idea of this project amazed me, and although unsure of its success, I'm eagerly anticipating seeing it come to fruition.


### Phub Tshering Doya
![Phub](../Images/doya.png)

My name is Phub Tshering Doya and I am 17 years old. I am very fond of exploring new things. I enjoy playing football and sprinting. I am a person who believes that where there is will there is a way, courage and determination takes us forward. I am a very resilient person. I am a person who sticks to principles, which makes me a good human being. Additionally I am very fond of working on certain projects and developing something if resources are provided. I was also mainly involved in finding information about this experiment and all the things that were to do with gathering information and writing works. From the project I hope to learn more about the duality of light particles and as I am more engaged in research and writing works I hope to get some extra information about the experiment. Additionally, I hope to learn how periscope works as we are using it in our experiment.


### Yeshey Dorji
![Yeshey](../Images/yeshey.png)

Hi, I'm Yeshey Dorji, a 16-year-old with a passion for sports like football and basketball. Alongside my love for playing on the field, I thrive on solving mathematical problems, finding joy in the logic and challenge they offer. Moreover, I'm captivated by the technology field, constantly amazed by its advancements and eager to delve deeper into its complexities. I see the potential to merge these interests, perhaps exploring the analytical side of sports through mathematical models or diving into technology-driven solutions for enhancing athletic performance. With each endeavor, I aim to grow not only in knowledge but also in skills that will shape my future pursuits and contribute positively to the world around me. I was mainly involved in doing the coding and putting things together provided by my peers. In this project, my main focus is to understand how light works. Light has a kind of split personality, which means it can act in different ways. I want to learn about these behaviors and how tiny particles called photons behave when they make up light. It's like discovering the secret life of light and its little building blocks.


### Peer Learning Strategies
<b>Task division</b>: Within our group we divided works among us ensuring the involvement of all our group members and making sure everyone gets to learn and experience something throughout the process

<b>Collaboration</b>: In group we used this technique for peer learning by giving each other collaborative hands and working together with an open mind.

<b>Open communication</b>: In our group we maintained open communication where everyone could share our own thoughts and ideas whilst working together throughout the process.

### Group Photo 
![Group](../Images/group.png)

[<span style="color: black;">Home</span>](./index.md) | [<span style="color: black;">Projects</span>](./project_idea.md) | [<span style="color: black;">Team</span>](./team.md) | [<span style="color: black;">Terminologies</span>](./terminologies.md) | [<span style="color: black;">Progress</span>](./progress.md)
