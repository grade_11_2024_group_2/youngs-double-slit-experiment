[<span style="color: black;">Home</span>](./index.md) | [<span style="color: black;">Project</span>](./project_idea.md) | [<span style="color: black;">Team</span>](./team.md) | [<span style="color: black;">Terminologies</span>](./terminologies.md) | [<span style="color: black;">Progress</span>](./progress.md)


# Terminologies


<b>Diffraction</b>: The bending of waves around obstacles or through openings, such as slits, leading to the spreading out of the wavefront.

<b>Interference</b>: The phenomenon where waves overlap and either reinforce (constructive interference) or cancel out (destructive interference) each other.

<b>Double Slit</b>: A barrier with two closely spaced parallel slits through which light passes, causing diffraction and interference.

<b>Interference Pattern</b>: The pattern of alternating bright and dark fringes observed on a screen when waves interfere constructively and destructively after passing through the double slit.

<b>Constructive Inteference</b>: When waves from different sources arrive at the same point with their crests (peaks) or troughs (valleys) aligned, resulting in an increase in amplitude.
Destructive Interference: When waves from different sources arrive at the same point with their crests (peaks) of one wave aligning with the troughs (valleys) of the other, resulting in a decrease in amplitude or cancellation.


## Links to watch videos related to this concept

[![Sample Video](https://www.learnatnoon.com/s/en-pk2/wp-content/uploads/sites/14/2022/10/young-slit-experiment.jpg)](https://www.youtube.com/watch?v=9UkkKM1IkKg)

To explore Young's Double Slit Experiment further, click on the image to watch an enlightening video explanation.

<br>



[![Sample Video](../Images/quantum.png)](https://www.youtube.com/watch?v=Ms-CVF540fo)


Explore a captivating video elucidating the intricacies of the Young's Double Slit Experiment 2.0 by simply clicking on the image.

[<span style="color: black;">Home</span>](./index.md) | [<span style="color: black;">Project</span>](./project_idea.md) | [<span style="color: black;">Team</span>](./team.md) | [<span style="color: black;">Terminologies</span>](./terminologies.md) | [<span style="color: black;">Progress</span>](./progress.md)
